using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEditor;
using UnityEngine.Networking;

public class CartSlotsContainer : MonoBehaviour
{
    //Create slots in the cart
    private RectTransform inventoryRect;
    private float inventoryWidth, inventoryHeight;
    public int slots;
    public int rows;
    public float sloatPaddingLeft, slotPaddingTop;
    public GameObject slotPrefab;
    public Transform slotContainer;

    //Create sum price
    [SerializeField] public TextMeshProUGUI sumList;
    public GameObject Slot;

    //Create slots list
    [SerializeField] private List<GameObject> allSlots;

    //Count empty slots
    public static int emptySlot;
    public static int EmptySlot
    {
        get
        {
            return emptySlot;
        }
        set
        {
            emptySlot = value;
        }
    }

    //ToJSON 
    public string savePath = "";
    public string filename = "testCart.json";

    private void Awake()
    {
        CreateLayout();  
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Summ();
    }

    //Create cart (slots)
    private void CreateLayout()
    {
        if (allSlots != null)
        {
            foreach (GameObject go in allSlots)
            {
                Destroy(go);
            }
        }

        allSlots = new List<GameObject>();

        emptySlot = slots;

        int columns = slots / rows;
        for (int x = 0; x < rows; x++)
        {
            for (int y = 0; y < columns; y++)
            {
                GameObject newSlot = (GameObject)Instantiate(slotPrefab);
                RectTransform slotRect = newSlot.GetComponent<RectTransform>();
                newSlot.name = "Slot";
                newSlot.transform.parent = GameObject.Find("SlotPanel").transform;
                slotRect.localPosition = new Vector3(0, 0, 0);
                allSlots.Add(newSlot);
            }

        }
    }

    //Find empty slot
    private bool PlaceEmpty(Item item)
    {
        if (emptySlot > 0)
        {
            foreach (GameObject slot in allSlots)
            {
                SlotController tmpSlot = slot.GetComponent<SlotController>();
                if (tmpSlot.isEmpty)
                {
                    tmpSlot.AddItem(item);
                    emptySlot--;
                    return true;
                }
            }
        }
        return false;
    }

    //Add item to slot + stack for product_name
    public bool AddItem(Item item)
    {
        if (item.maxSize == 1)
        {
            PlaceEmpty(item);
            return true;
        }
        else
        {
            foreach (GameObject slot in allSlots)
            {
                SlotController tmp = slot.GetComponent<SlotController>();
                if (!tmp.isEmpty)
                {
                    if (tmp.CurrentItem.product_name == item.product_name && tmp.isAvailable)
                    {
                        tmp.AddItem(item);
                        return true;
                    }
                }
            }
            if (emptySlot > 0)
            {
                PlaceEmpty(item);
            }
        }

        return false;
    }

    //Sum slots
    public void Summ()
    {
        //for (int i = 0; i < slotContainer.childCount; i++)
        //{
        //    string strCount = slotContainer.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text.ToString();
        //    int sumCount;
        //    bool success = Int32.TryParse(strCount, out sumCount);
        //    sumCount += strCount[i];
        //    Debug.Log(success);
        //   Debug.Log(sumCount);
        //    sumList.text = sumCount.ToString();
        //
        // }

        string strCount = slotContainer.GetChild(0).GetChild(2).GetComponent<TextMeshProUGUI>().text;
        string strCount1 = slotContainer.GetChild(1).GetChild(2).GetComponent<TextMeshProUGUI>().text;
        string strCount2 = slotContainer.GetChild(2).GetChild(2).GetComponent<TextMeshProUGUI>().text;
        string strCount3 = slotContainer.GetChild(3).GetChild(2).GetComponent<TextMeshProUGUI>().text;
        string strCount4 = slotContainer.GetChild(4).GetChild(2).GetComponent<TextMeshProUGUI>().text;
        string strCount5 = slotContainer.GetChild(5).GetChild(2).GetComponent<TextMeshProUGUI>().text;
        string strCount6 = slotContainer.GetChild(6).GetChild(2).GetComponent<TextMeshProUGUI>().text;
        string strCount7 = slotContainer.GetChild(7).GetChild(2).GetComponent<TextMeshProUGUI>().text;
        string strCount8 = slotContainer.GetChild(8).GetChild(2).GetComponent<TextMeshProUGUI>().text;
        string strCount9 = slotContainer.GetChild(9).GetChild(2).GetComponent<TextMeshProUGUI>().text;

        /*
        string strCount10 = slotContainer.GetChild(10).GetChild(1).GetComponent<TextMeshProUGUI>().text;
        string strCount11 = slotContainer.GetChild(11).GetChild(1).GetComponent<TextMeshProUGUI>().text;
        string strCount12 = slotContainer.GetChild(12).GetChild(1).GetComponent<TextMeshProUGUI>().text;
        string strCount13 = slotContainer.GetChild(13).GetChild(1).GetComponent<TextMeshProUGUI>().text;
        string strCount14 = slotContainer.GetChild(14).GetChild(1).GetComponent<TextMeshProUGUI>().text;
        string strCount15 = slotContainer.GetChild(15).GetChild(1).GetComponent<TextMeshProUGUI>().text;
        string strCount16 = slotContainer.GetChild(16).GetChild(1).GetComponent<TextMeshProUGUI>().text;
        string strCount17 = slotContainer.GetChild(17).GetChild(1).GetComponent<TextMeshProUGUI>().text;
        string strCount18 = slotContainer.GetChild(18).GetChild(1).GetComponent<TextMeshProUGUI>().text;
        string strCount19 = slotContainer.GetChild(19).GetChild(1).GetComponent<TextMeshProUGUI>().text;
        */

        int summ = int.Parse(strCount) +
            int.Parse(strCount1) +
            int.Parse(strCount2) +
            int.Parse(strCount3) +
            int.Parse(strCount4) +
            int.Parse(strCount5) +
            int.Parse(strCount6) +
            int.Parse(strCount7) +
            int.Parse(strCount8) +
            int.Parse(strCount9);
        //int.Parse(strCount10) +
        //int.Parse(strCount11) +
        //int.Parse(strCount12) +
        //int.Parse(strCount13) +
        //int.Parse(strCount14) +
        //int.Parse(strCount15) +
        //int.Parse(strCount16) +
        //int.Parse(strCount17) +
        //int.Parse(strCount18) +
        //int.Parse(strCount19);

        sumList.text = summ.ToString();
        //Debug.Log(summ);
    }

    //Save to JSON
    public void Save()
    {
        List<DataSlot> dataSlots = new List<DataSlot>();
        for (int i = 0; i < allSlots.Count; i++)
        {
            dataSlots.Add(new DataSlot(
                slotContainer.GetChild(i).GetChild(1).GetComponent<TextMeshProUGUI>().text,
                slotContainer.GetChild(i).GetChild(2).GetComponent<TextMeshProUGUI>().text,
                slotContainer.GetChild(i).GetChild(3).GetComponent<TextMeshProUGUI>().text
                ));
        }
        ListSlot listSlot = new ListSlot(dataSlots);
        string json = JsonUtility.ToJson(listSlot);
        File.WriteAllText(savePath + filename, json);
        //Debug.Log(json);
        // Debug.Log(savePath);
        //Debug.Log(filename);

        //StartCoroutine(PostJSON("http://localhost:8000/app", json));
        Debug.Log(json);
    }

    [System.Serializable]
    public struct DataSlot
    {
        public string m_name;
        public string m_price;
        public string m_count;

        public DataSlot(string M_name, string M_price, string M_count)
        {
            m_name = M_name;
            m_price = M_price;
            m_count = M_count;
        }
    }

    public struct ListSlot
    {
        public List<DataSlot> dataSlots;
        public ListSlot(List<DataSlot> someDatas)
        {
            dataSlots = someDatas;
        }
    }
}
