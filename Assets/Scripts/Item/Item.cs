using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    public Sprite spriteNeutral;
    public Sprite spriteHeighlighted;
    public int maxSize;
    public string product_name;
    public int price;
    public string description;
}
